import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  favorites: [],
};

export const FavoriteSlice = createSlice({
  name: 'favorites',
  initialState,
  reducers: {
    addFavorite: (state, action) => {
      state.favorites = [...state.favorites, action.payload];
    },
    removeFavorite: (state, action) => {
      const newArr = state.favorites.filter(
        (item) => item.name !== action.payload.name
      );
      state.favorites = [...newArr];
    },
  },
});

export const favoriteActions = FavoriteSlice.actions;

export default FavoriteSlice;
