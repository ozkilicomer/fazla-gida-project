import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isValid: false,
};

export const AuthSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    loginValidation: (state, action) => {
      state.isValid = action.payload;
    },
  },
});

export const { loginValidation } = AuthSlice.actions;

export default AuthSlice;
