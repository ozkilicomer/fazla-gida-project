export default {
  auth: {
    expireTime: 0,
    lastAuthAt: 0,
    isValid: false,
    isApiValid: false,
    accessToken: '',
    apiAccessToken: '',
    refreshToken: null,
  },
};
