import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

function useFetch(page) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [artistsList, setArtistsList] = useState([]);

  const sendQuery = useCallback(async () => {
    try {
      setLoading(true);
      setError(false);
      const params = {
        api_key: process.env.REACT_APP_API_KEY,
        language: localStorage.getItem('language'),
        page: page,
        limit: 9,
        format: 'json',
      };
      const res = await axios.get(
        `http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists`,
        {
          params,
        }
      );
      setArtistsList((prev) => [
        ...new Set([...prev, ...(res?.data?.artists.artist ?? [])]),
      ]);
      setLoading(false);
    } catch (err) {
      setError(err);
    }
  }, [page]);

  useEffect(() => {
    sendQuery();
  }, [sendQuery, page]);

  return { loading, error, artistsList };
}

export default useFetch;
