import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { loginValidation } from '../../../app/slices/AuthSlice';
import { uiActions } from '../../../app/slices/uiSlice';
import { langActions } from '../../../app/slices/langSlice';
import Auth from '../../../libraries/Auth';
import { Sun, MoonStars, AB } from 'tabler-icons-react';
import { Header, Group, ActionIcon, Button, Text } from '@mantine/core';
import ThemeProviderContext from '../../../app/context/ThemeContext';
import { darkTheme, lightTheme } from '../../../styles/theme';
import { useTranslation } from 'react-i18next';

const HeaderMain = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();
  const currentLanguage = useSelector((state) => state.lang.currentLang);
  const ui = useContext(ThemeProviderContext);

  const toggleThemeHandler = () => {
    dispatch(uiActions.toggleTheme());
  };

  const toggleLanguage = () => {
    dispatch(langActions.toggleLanguage());
    location.reload();
  };

  const logout = () => {
    Auth.logout();
    dispatch(loginValidation(false));
    history.push('/login');
  };

  return (
    <Header
      sx={{
        backgroundColor: ui.theme === 'light' ? lightTheme.bg : darkTheme.bg,
      }}
      height={60}
    >
      <Group sx={{ height: '100%' }} px={20} position="apart">
        <Group>
          <ActionIcon variant="outline" size={30}>
            <AB />
          </ActionIcon>
          <Text size="lg" weight={500} variant="link" component="a" href="/">
            Project
          </Text>
        </Group>

        <ActionIcon
          variant="default"
          onClick={() => toggleThemeHandler()}
          size={30}
        >
          {ui.theme === 'light' ? <MoonStars size={16} /> : <Sun size={16} />}
        </ActionIcon>
        <ActionIcon
          variant="default"
          onClick={() => toggleLanguage()}
          size={30}
        >
          {currentLanguage === 'tr' ? (
            <Sun size={16} />
          ) : (
            <MoonStars size={16} />
          )}
        </ActionIcon>
        <Button onClick={logout}>{t('logout')}</Button>
      </Group>
    </Header>
  );
};

export default HeaderMain;
