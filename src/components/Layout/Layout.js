import { ScrollArea } from '@mantine/core';
import React from 'react';

const Layout = ({ children }) => {
  return (
    <ScrollArea
      offsetScrollbars
      style={{ height: 'calc(100vh - 92px)' }}
      type="hover"
    >
      {children}
    </ScrollArea>
  );
};

export default Layout;
