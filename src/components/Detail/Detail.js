import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import {
  Paper,
  Title,
  Grid,
  Image,
  Input,
  Button,
  ThemeIcon,
  Divider,
} from '@mantine/core';
import { useTranslation } from 'react-i18next';
import { CircleCheck, Search } from 'tabler-icons-react';
import ListItem from '../Parts/ListItem';

const Detail = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const [detail, setDetail] = useState({
    isLoading: false,
    selected: history.location.state?.item ?? null,
    topAlbums: [],
    topTracks: [],
  });
  const [searchText, setSearchText] = useState('');
  const [searchData, setSearchData] = useState([]);
  console.log('history', history);
  const nameArtist = history.location.pathname.split('/')[2];

  useEffect(() => {
    if (nameArtist === undefined) {
      return;
    } else {
      fetchDetail();
    }
  }, []);

  const fetchDetail = async (searchName) => {
    setDetail((prevState) => ({ ...prevState, isLoading: true }));

    try {
      const params = {
        api_key: process.env.REACT_APP_API_KEY,
        language: localStorage.getItem('language'),
        artist: nameArtist ?? searchName,
        format: 'json',
        limit: 10,
      };
      const resTopAlbums = await axios.get(
        `http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums`,
        {
          params,
        }
      );
      const resTopTracks = await axios.get(
        `http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks`,
        {
          params,
        }
      );
      setDetail((prevState) => ({
        ...prevState,
        isLoading: false,
        topAlbums: resTopAlbums.data.topalbums.album,
        topTracks: resTopTracks.data.toptracks.track,
      }));
    } catch (err) {
      setDetail((prevState) => ({
        ...prevState,
        isLoading: false,
        topAlbums: [],
        topTracks: [],
      }));
    }
  };

  const fetchSearch = () => {
    const params = {
      api_key: process.env.REACT_APP_API_KEY,
      language: localStorage.getItem('language'),
      artist: searchText,
      format: 'json',
      limit: 10,
    };
    axios
      .get(`http://ws.audioscrobbler.com/2.0/?method=artist.search`, {
        params,
      })
      .then((res) => {
        setSearchData(res?.data?.results.artistmatches.artist);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return nameArtist === undefined ? (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('search')}
      </Title>
      <Grid sx={{ marginBottom: '2rem' }}>
        <Grid.Col span={10}>
          <Input
            icon={<Search size={16} />}
            placeholder={t('search')}
            rightSectionWidth={70}
            styles={{ rightSection: { pointerEvents: 'none' } }}
            onChange={(event) => {
              setSearchText(event.target.value);
            }}
          />
        </Grid.Col>
        <Grid.Col span={2}>
          <Button
            variant="light"
            color="blue"
            fullWidth
            disabled={searchText === ''}
            onClick={() => {
              fetchSearch();
            }}
          >
            {t('search')}
          </Button>
        </Grid.Col>
      </Grid>
      <Paper shadow="md" radius="md" p="md">
        {searchData.map((item, index) => {
          return (
            <Grid sx={{ alignItems: 'center' }} key={index}>
              <Grid.Col span={2}>
                <ThemeIcon color="teal" size={24} radius="xl">
                  <CircleCheck size={16} />
                </ThemeIcon>
              </Grid.Col>
              <Grid.Col span={8}>{item.name}</Grid.Col>
              <Grid.Col span={2}>
                <Button
                  variant="light"
                  color="blue"
                  fullWidth
                  onClick={() => {
                    history.push(`/detail/${item.name}`, { item: item });
                    setDetail((prevState) => ({
                      ...prevState,
                      selected: item,
                    }));
                    fetchDetail(item.name);
                  }}
                >
                  {t('detail')}
                </Button>
              </Grid.Col>
            </Grid>
          );
        })}
      </Paper>
    </>
  ) : (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('detail')}
      </Title>
      <Paper shadow="md" radius="md" p="md">
        <Grid>
          <Grid.Col span={4}>
            <Image
              src={`${detail.selected.image[1]['#text']}`}
              height={100}
              alt={detail?.selected?.title}
            />
          </Grid.Col>
          <Grid.Col span={8}>
            <Title align="center" sx={{ marginBottom: '2rem' }} order={1}>
              {detail?.selected?.name ?? ''}
            </Title>
            <Grid>
              <Grid.Col span={6}>
                <Title align="center" sx={{ color: '#3BC9DB' }} order={4}>
                  {t('listeners')}
                </Title>
                <Title
                  align="center"
                  sx={{ color: '#F06595' }}
                  color="leaf"
                  order={3}
                >
                  {detail?.selected?.listeners ?? 0}
                </Title>
              </Grid.Col>
              <Grid.Col span={6}>
                <Title align="center" order={4} sx={{ color: '#3BC9DB' }}>
                  {t('playCount')}
                </Title>
                <Title align="center" order={3} sx={{ color: '#F06595' }}>
                  {detail?.selected?.playcount ?? 0}
                </Title>
              </Grid.Col>
            </Grid>
          </Grid.Col>
        </Grid>
        <Grid sx={{ marginTop: '2rem' }}>
          <Grid.Col span={6}>
            <Title sx={{ marginBottom: '2rem' }} order={1}>
              {t('topAlbums')}
            </Title>
            <Divider my="sm" variant="dashed" />
            <ListItem data={detail?.topAlbums ?? []} />
          </Grid.Col>
          <Grid.Col span={6}>
            <Title sx={{ marginBottom: '2rem' }} order={1}>
              {t('topTracks')}
            </Title>
            <Divider my="sm" variant="dashed" />
            <ListItem data={detail?.topTracks ?? []} />
          </Grid.Col>
        </Grid>
      </Paper>
    </>
  );
};

export default Detail;
