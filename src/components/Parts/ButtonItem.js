import { Button } from '@mantine/core';
import React from 'react';

const ButtonItem = (props) => {
  const { onClick, label } = props;

  return (
    <Button data-testid="button" onClick={onClick} {...props}>
      {label}
    </Button>
  );
};

export default ButtonItem;
