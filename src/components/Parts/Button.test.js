import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Button from './ButtonItem';

test('button renders correctly', () => {
  const { getByTestId } = render(<Button />);

  expect(getByTestId(/button/i)).toBeInTheDocument();
});
