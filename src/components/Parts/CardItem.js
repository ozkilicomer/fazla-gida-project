import { Card, Grid, Image, Text, useMantineTheme, Title } from '@mantine/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Heart } from 'tabler-icons-react';
import { favoriteActions } from '../../app/slices/FavoriteSlices';
import { lightTheme } from '../../styles/theme';
import ButtonItem from './ButtonItem';

const CardItem = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const { item } = props;
  const favorites = useSelector((state) => state.favorite.favorites);
  const theme = useMantineTheme();
  const dispatch = useDispatch();

  const addFav = (item) => {
    dispatch(favoriteActions.addFavorite(item));
  };
  return (
    <Card
      sx={{
        backgroundColor:
          theme.colorScheme === 'dark' ? theme.colors.dark[1] : lightTheme.bg,
      }}
      shadow="sm"
      p="lg"
    >
      <Card.Section>
        <Image
          src={`${item.image[2]['#text']}`}
          height={250}
          alt={item.title}
          fit="contain"
        />
      </Card.Section>
      <Text
        style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
        align="center"
        weight={500}
      >
        {item?.name ?? ''}
      </Text>

      <Grid>
        <Grid.Col span={6}>
          <Title align="center" sx={{ color: '#3BC9DB' }} order={4}>
            {t('listeners')}
          </Title>
          <Title
            align="center"
            sx={{ color: '#F06595' }}
            color="leaf"
            order={3}
          >
            {item?.listeners ?? 0}
          </Title>
        </Grid.Col>
        <Grid.Col span={6}>
          <Title align="center" order={4} sx={{ color: '#3BC9DB' }}>
            {t('playCount')}
          </Title>
          <Title align="center" order={3} sx={{ color: '#F06595' }}>
            {item?.playcount ?? 0}
          </Title>
        </Grid.Col>
      </Grid>

      <Grid>
        <Grid.Col span={6}>
          <ButtonItem
            variant="light"
            color="blue"
            fullWidth
            style={{ marginTop: 14 }}
            onClick={() => {
              history.push(`/detail/${item.name}`, { item: item });
            }}
            label={t('detail')}
          />
        </Grid.Col>
        <Grid.Col span={6}>
          <ButtonItem
            variant="light"
            color="pink"
            fullWidth
            style={{ marginTop: 14 }}
            onClick={() => {
              if (favorites.find((fav) => fav.name === item.name)) {
                dispatch(favoriteActions.removeFavorite(item));
              } else {
                addFav(item);
              }
            }}
            label={
              <Heart
                fill={
                  favorites.some((itemFav) => itemFav.name === item.name)
                    ? 'red'
                    : 'none'
                }
                color="red"
                size={16}
              />
            }
          ></ButtonItem>
        </Grid.Col>
      </Grid>
    </Card>
  );
};

export default CardItem;
