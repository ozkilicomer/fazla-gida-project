import React, { Suspense } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import Layout from '../components/Layout/Layout';
import Box from '../libraries/Box';
import routes from '../routes';

const loading = <div>loading...</div>;

const Content = () => {
  const auth = useSelector((state) => state.auth);

  return (
    <Suspense fallback={loading}>
      <Layout>
        <Switch>
          {routes.map((route, idx) => {
            return route.component ? (
              <Route
                key={idx}
                path={route.path}
                exact={route.exact}
                name={route.name}
                render={(props) =>
                  Box.auth.isValid && auth?.isValid ? (
                    <route.component {...props} />
                  ) : (
                    <Redirect to="/login" />
                  )
                }
              />
            ) : null;
          })}
          <Redirect from="/" to="/" />
        </Switch>
      </Layout>
    </Suspense>
  );
};

export default React.memo(Content);
