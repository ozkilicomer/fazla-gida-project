export const lightTheme = {
  bg: '#fff',
};

export const darkTheme = {
  bg: '#5C5F66',
};
